package chat

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
	"time"
	//"github.com/astaxie/session"
	//_ "github.com/astaxie/session/providers/memory"
	//"github.com/astaxie/session"
	"math/rand"
)

//过期时间
const LEFTTIME int64 = 3600

//聊天类型
type Chat struct {
	Db *sql.DB
}

//单条消息类型
type Message struct {
	Chat_id  int64
	From_key string
	To_key   string
	Content  string
	Add_time int64
}

//在线用户
type Client struct {
	//session string
	Lefttime int64  `json:"left_time"`
	Nickname string `json:"nick_name"`
}

//测试
func (c *Chat) Test() {
	fmt.Println("test chat lib")
}

//发送消息
func (c *Chat) Send(content string, from_key string, to_key string) (id int64, err error) {
	//fmt.Println(content,from_key,to_key)
	id = 0
	err = errors.New("")
	if content == "" {
		err = errors.New("内容不能为空")
		return
	}
	//fmt.Println(c.Db)
	insert, err1 := c.Db.Prepare("INSERT go_chat SET from_key=?,to_key=?,content=?,add_time=?")
	//panic(err)
	if err1 != nil {
		err = err1
		return
	}

	res, err2 := insert.Exec(from_key, to_key, content, time.Now().Unix())
	if err2 != nil {
		err = err2
		return
	}
	id, err3 := res.LastInsertId()
	if err3 != nil {
		err = err3
		return
	}
	return
}

//列表消息
func (c *Chat) List(to_key string, chat_id int64, limit int) (List []Message, err error) {
	rows, err1 := c.Db.Query("SELECT * FROM go_chat WHERE (to_key='" + to_key + "' OR to_key='' ) AND chat_id>'" + strconv.FormatInt(chat_id, 10) + "' ORDER BY chat_id DESC LIMIT " + strconv.Itoa(limit))
	if err1 != nil {
		panic(err1)
		return
	}
	//fmt.Println(message)
	//message = []message_one{}
	defer rows.Close()
	for rows.Next() {
		//var content string
		//err = rows.Scan(&content)
		//checkError(err)
		//fmt.Println(content)
		var m Message
		err2 := rows.Scan(&m.Chat_id, &m.From_key, &m.To_key, &m.Content, &m.Add_time)
		//fmt.Println(m)
		if err2 == nil {
			List = append(List, m)
		}
	}
	//fmt.Println(List)
	//fmt.Println("------------------------")
	//for _,v := range List{
	//	fmt.Println(v)
	//}
	//fmt.Println("------------------------")
	return
}

//在线人
func Online(who map[string]Client, sessionid string) map[string]Client {
	var wc Client
	if _, ok := who[sessionid]; ok {
		wc = who[sessionid]
		wc.Lefttime = time.Now().Unix() + LEFTTIME
	} else {
		wc = Client{Lefttime: time.Now().Unix() + LEFTTIME, Nickname: "游客" + strconv.Itoa(rand.Intn(10000000))}
	}
	who[sessionid] = wc
	dealWho(who)
	return who
}

//处理在线人
func dealWho(who map[string]Client) map[string]Client {
	for i, v := range who {
		if v.Lefttime < time.Now().Unix() {
			delete(who, i)
		}
	}
	return who
}

//改名
func (c *Chat)ChangeName(who map[string]Client, sessionid string, nickname string) map[string]Client {
	if _, ok := who[sessionid]; ok {
		temp := who[sessionid]
		temp.Nickname = nickname
		who[sessionid] = temp
		c.Send("我修改名字为 "+nickname,sessionid,"")
	}
	return who
}
