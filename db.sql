-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: goserver
-- ------------------------------------------------------
-- Server version	5.6.28-1ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `goserver`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `goserver` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `goserver`;

--
-- Table structure for table `go_chat`
--

DROP TABLE IF EXISTS `go_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `go_chat` (
  `chat_id` int(10) NOT NULL AUTO_INCREMENT,
  `from_key` char(60) COLLATE utf8_unicode_ci NOT NULL,
  `to_key` char(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`chat_id`),
  KEY `from_key` (`from_key`,`to_key`),
  KEY `from_key_2` (`from_key`,`to_key`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `go_chat`
--

LOCK TABLES `go_chat` WRITE;
/*!40000 ALTER TABLE `go_chat` DISABLE KEYS */;
INSERT INTO `go_chat` VALUES (1,'123dd','dfadf','test',0),(2,'123dd','dfadf','test',12),(3,'','','123',123),(4,'','','123',123),(5,'','','123',123),(6,'','','123',123),(7,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','123',123),(8,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','123',123),(9,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','123',123),(10,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','测试内容',123),(11,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','测试内容3',123),(12,'BtYyWrLaFfd39KbBX1rkyp9j4CKPD4zmhqy6EKDuqB0%3D','','测试内容6',123);
/*!40000 ALTER TABLE `go_chat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-05 15:31:03
