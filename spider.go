package main

import (
	"fmt"
	"github.com/astaxie/goredis"
	"net/http"
	"regexp"
	"io/ioutil"
	"strings"
)

var redis goredis.Client

var total int

func get_body(url string){
	if url == ""{
		_,b1,_ := redis.Blpop([]string{"url_cache"},10)
		fmt.Println("cache_url:",string(b1))
		url = string(b1)
	}
//	client := &http.Client{}
	//req,err := http.NewRequest("GET",s.url,nil)
//	fmt.Println("find url:",url)
	ok,_:= redis.Hget("url_data",url)
//	CheckError(err)
	if len(ok)>0{
		get_body("")
		return
	}
//	http.Header.Set("Referer","http://www.phpec.org")
	resp,err := http.Get(url)
	if err != nil{
		get_body("")
		return
	}
	defer resp.Body.Close()
	//text/html; charset=utf-8
	content_type := resp.Header.Get("Content-Type")
	if string([]byte(content_type)[0:9]) != "text/html"{
		get_body("")
		return
	}
	data,err := ioutil.ReadAll(resp.Body)
//	CheckError(err)
	if err != nil{
		get_body("")
		return
	}
//	fmt.Println(url)
	var body string = string(data)
	//fmt.Println(body)
	redis.Hset("url_data",url,data)
	r,_ := regexp.Compile("<a(.*?)href=('|\")(.*?)('|\")")
	match := r.FindAllStringSubmatch(body,-1)
//	fmt.Println(match)
	path,host := GetPath(url)
	fmt.Println("path:",path,"host",host)
	for _,suburl := range match{
		full_url := DealUrl(suburl[3],path,host)
		if full_url == ""{
			continue
		}
		fmt.Println("full_url :", full_url)
		total++
//		fmt.Println("get url ",real_url)
		finded,_ := redis.Hget("url_list", full_url)
		if(len(finded)==0) {
			redis.Lpush("url_cache", []byte(full_url))
		}
		redis.Hset("url_list",full_url,[]byte("0"))
	}
	get_body("")
	return
}

func DealUrl(url string,path string,host string)string{
	if url ==""{
		return ""
	} else if url[0] == '#'{
		return ""
	}else if url[0] == '/'{
		return host+url
	}else if match,_ := regexp.MatchString("^(http://|https://)",url);match{
		return url
	}else if match,_ := regexp.MatchString("^(javascript:;)",url);match{
		return ""
	} else {
		return path+url
	}
	return url
}

func GetPath(url string)(path string,host string){
	query := strings.Split(url,"#")
	query2 := strings.Split(query[0],"/")
//	fmt.Println(query2)
	host = query2[0]+"/"+query2[1]+"/"+query2[2]
	if len(query2) == 3{
		path = host
	} else {
		//	delete(query2,len(query2)-1)
		query3 := query2[:len(query2) - 1]
		path = strings.Join(query3, "/")
	}
	if path[len(path)-1] != '/'{
		path += "/"
	}
	return
}

func InitRedis(){
	//数据库连接
	redis.Addr = "127.0.0.1:6379"
	//选择DB
	redis.Db = 3
}


func CheckError(err error){
	if err != nil {
		panic(err)
	}
}

func main(){
	InitRedis()
	//client.Set("test",[]byte("dddd"));
	fmt.Println("robot..")
//	fmt.Println(GetPath("http://c.biancheng.net/cpp/html/1435.html"))
	get_body("http://www.phpec.org")

}

