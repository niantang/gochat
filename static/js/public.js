/**
 * Created by aboc on 2016/1/20.
 */
var CHAT = {
    last_id:0,
    name:'游客',
    session:'',
    who:{}
};
$(function(){

    $("#to_chat").click(function(){
        var content = $("#chat_content").html();
        //console.info(content);
        if(content == ""){
            show_msg("聊天内容不能为空");
        }
        _to_chat(content,$("#to_key").val(),false);
        return false;
    });
    $("#change_name").click(function(){
        var t = layer.prompt({
            title: '请输入您的新名字',
            formType: 0,
            value:CHAT.name
        }, function(value){
            if(value==""){
                show_msg("新名字不能为空")
                return;
            }
            $.ajax({
                url:"/chat/changename",
                dataType:"json",
                type:"post",
                data:{name:value},
                success:function(d){
                    console.info(d)
                    if(d.err != ""){
                        show_msg(d.err);
                    } else {
                        console.info(d.msg)
                        layer.msg(d.msg);
                        $("#nick_name").text(value)
                        layer.close(t)
                    }
                }
            });
        });
        return false;
    });
    setTimeout(function(){
        $("body").scrollTop($(document).height());
    },100);
    load_msg();
    setInterval(function(){
        load_msg();
    },3000);
});



function show_msg(error,msg,second){
    error = error || "";
    msg = msg || "";
    second = second || 3;
    if(error !=""){
        layer.msg(error,function(){});
    }
    if(msg != ""){
        layer.msg(msg);
    }
}


function deal_msg(msg_list){
    if(msg_list) {
        var a = [];
        msg_list.reverse()
        $.each(msg_list, function (i, v) {
            if (v.chat_id && $(".chat_main .list dl[data-id=" + v.chat_id + "]").length) {
                var p = $(".chat_main .list dl[data-id=" + v.chat_id + "]");
                $("dt em", $(p)).html(v.format_time);
            }
            var name = typeof CHAT.who[v.from_key] != "undefined" ? CHAT.who[v.from_key].nick_name : "游客";
            var str = '<dl class="one ' + (v.is_me ? 'is_me' : '') + '" data-id="' + v.chat_id + '" data-dom="' + (v.dom || rand(6)) + '">'
                + '<dt><span>' + name + '</span><em>' + v.format_time + '</em></dt>'
                + '<dd>' + v.content + '</dd>'
                + '</dl>';
            a.push(str);
            CHAT.last_id = v.chat_id;
        });
        $(".chat_main .list").append(a.join(""));
    }
}

function rand(len,type) {
    len = len < 0 ? 0 : len;
    type = type && type<=3? type : 3;
    var str = '';
    for (var i = 0; i < len; i++) {
        var j = Math.ceil(Math.random()*type);
        if (j == 1) {
            str += Math.ceil(Math.random()*9);
        } else if (j == 2) {
            str += String.fromCharCode(Math.ceil(Math.random()*25+65));
        } else {
            str += String.fromCharCode(Math.ceil(Math.random()*25+97));
        }
    }
    return str;
}

function _to_chat(content,to_key,try_send) {
    var dom = rand(8);
    to_key = to_key || "";
    if(typeof try_send == "undefined"){
        try_send = false;
    }
    if(!try_send) {
        deal_msg([{
            chat_id: 0,
            from_key: CHAT.session,
            is_me: true,
            content: content,
            add_time: 0,
            format_time: "刚刚",
            dom: dom,
        }]);
        $("#chat_content").html("");
    }
    $.ajax({
        url: "/chat/send",
        dataType: "json",
        type: "post",
        data: {content: content,to_key:to_key},
        success: function (d) {
            if (d.chat_id) {
                $(".chat_main .list dl[data-dom=" + dom + "]").attr("data-id", d.chat_id);
            }
            deal_msg(d.list)
            if(d.chat_id){
                CHAT.last_id = d.chat_id;
            }
            if(!try_send){
                $("body").scrollTop($(document).height());
            }
        },
        error: function (d) {

        }
    });
}

function load_msg(){
    $.ajax({
        url: "/chat/list",
        dataType: "json",
        type: "get",
        data: {chat_id:CHAT.last_id},
        success: function (d) {
            if(d.status == 0) {
                if(typeof d.who != "undefined"){
                    CHAT.who = d.who;
                    CHAT.name = d.who[d.current].nick_name;
                }
                CHAT.session = d.current;
                $("#nick_name").text(CHAT.name)
                deal_msg(d.list);
            }
        },
        error: function (d) {

        }
    });
}