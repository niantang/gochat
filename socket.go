package main

import (
	"fmt"
	"net/http"
	//"time"
	"code.google.com/p/go.net/websocket"
	"log"
)

func Echo(ws *websocket.Conn){
	var err error
	for {
		var reply string
		if err = websocket.Message.Receive(ws,&reply);err != nil{
			fmt.Println("Can't receive")
			break
		}
		fmt.Println("from client :"+reply)
		msg := "Recevied :"+reply
		fmt.Println("to client :"+msg)
		if err = websocket.Message.Send(ws,msg);err != nil{
			fmt.Println("Can't send")
			break
		}
	}
}

func main(){

	http.Handle("/",websocket.Handler(Echo))
	if err := http.ListenAndServe(":9191",nil);err != nil{
		log.Fatal("ListenAndServer:",err)
	}
}

func sCheckError(err error){
	if err != nil{
		panic(err)
	}
}

/**

var ws = new WebSocket("ws://localhost:9191");
ws.onopen = function()
{
  console.log("open");
  ws.send("dsfasdf=123&kuooo=345");
};
ws.onmessage = function(evt)
{
  console.log(evt.data)
};
ws.onclose = function(evt)
{
  console.log("WebSocketClosed!");
};
ws.onerror = function(evt)
{
  console.log("WebSocketError!");
};


 */