package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	//	"reflect" //获取类型用到
	"database/sql"
	"encoding/json"
	"github.com/astaxie/session"
	_ "github.com/astaxie/session/providers/memory"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	//"time"
	"goserver/chat"
	"time"
)

/**
端口
*/
var port int = 9090

//get
var get map[string][]string

//路由
var new_query []string

//map  没什么用
type TheMap struct {
	TheString []string
	TheInt    []int
	TheFloat  []float32
}

//html  测试用的
type Html struct {
	ThemesName string
	TheMap
	Age int
}

var sess session.Session

//session id
var sessionid string

var fdb *sql.DB

var rw http.ResponseWriter
var rr *http.Request

var globalSessions *session.Manager

var cc chat.Chat

/**
 * 清除slice 里面空的
 */
func clearEmpty(array []string) []string {
	var data []string
	for _, v := range array {
		if v != "" {
			data = append(data, v)
		}
	}
	return data
}

/**
路由入口
*/
func www(w http.ResponseWriter, r *http.Request) {
	rw = w
	rr = r
	r.ParseForm()
	//fmt.Println("visitor www")

	//fmt.Println(r.RequestURI)
	query_url := strings.SplitN(r.RequestURI, "?", 2)
	query := strings.SplitN(query_url[0], "/", 100)
	//fmt.Println("===========>",query[1])
	//fmt.Println(r.URL.Path)

	get = make(map[string][]string)
	//	fmt.Println(get)
	for k, v := range r.Form {
		get[k] = v
		//fmt.Println(k,v)
	}
	new_query = clearEmpty(query)
	fmt.Println(new_query)
	if i := len(new_query); i >= 1 {
		if new_query[0] == "log" {
			//initSession()
			wwwLog()
			return
		} else if new_query[0] == "login" {
			initSession()
			wwwLogin()
			return
		} else if new_query[0] == "upload" {
			wwwUpload()
			return
		} else if new_query[0] == "chat" {
			initSession()
			if len(new_query) > 1 {
				if new_query[1] == "send" {
					wwwChatSend()
					return
				} else if new_query[1] == "changename" {
					wwwChatChangeName()
					return
				} else if new_query[1] == "who" {
					wwwChatWho()
					return
				} else if new_query[1] == "list" {
					wwwChatList()
					return
				}
			}
			wwwChat()
			return
		} else if new_query[0] == "static" {
			http.Handle("/static/", http.FileServer(http.Dir("./")))
			return
		} else if new_query[0] == "session" {
			initSession()
			wwwSession()
			return
		}

	} else {

	}

	fmt.Fprintf(w, "d89392296a98e35c70b2341efcc84616")
}

/**
  log
*/
func wwwLog() {
	// r.ParseForm()
	//fmt.Println("visitor log")
	fmt.Fprintf(rw, "Hello log")
}

/**
测试上传的
*/
func wwwUpload() {
	if rr.Method == "GET" {
		t := template.New("upload.html")
		t, _ = t.ParseFiles("themes/upload.html")
		html := Html{ThemesName: "upload"}
		t.Execute(rw, html)
	} else {

	}

}

/**
  登录的
*/
func wwwLogin() {
	t := template.New("login.html")
	//	themes := [1]string{"themes/login.html"}
	t, _ = t.ParseFiles("themes/login.html")
	html := Html{ThemesName: "login", TheMap: TheMap{TheString: []string{"a1", "b1", "c1"}, TheInt: []int{1, 2, 3, 4}, TheFloat: []float32{1.2, 1.3, 1.4, 1.5, 1.6}}, Age: 18}
	fmt.Println("login themes")
	//	fmt.Println(html)
	t.Execute(rw, html)
}

/**
聊天的
*/
func wwwChat() {
	t := template.New("chat.html")
	//	themes := [1]string{"themes/login.html"}
	t, _ = t.ParseFiles("themes/chat.html")
	html := Html{ThemesName: "chat", TheMap: TheMap{TheString: []string{"a1", "b1", "c1"}, TheInt: []int{1, 2, 3, 4}, TheFloat: []float32{1.2, 1.3, 1.4, 1.5, 1.6}}, Age: 18}
	//fmt.Println("chat..",html)
	t.Execute(rw, html)
}

/**
输出消息的类型
*/
type outputMsg struct {
	Status int    `json:"status"`
	Err    string `json:"err"`
	Msg    string `json:"msg"`
}

/**
  消息的json格式类型
*/
type MessageJson struct {
	Chat_id     int64  `json:"chat_id"`
	From_key    string `json:"from_key"`
	To_key      string `json:"to_key"`
	Content     string `json:"content"`
	Add_time    int64  `json:"add_time"`
	Format_time string `json:"format_time"`
	Is_me       bool   `json:"is_me"`
}

/**
消息列表的格式类型
*/
type MessageList struct {
	Status  int   `json:"status"`
	Chat_id int64 `json:"chat_id"`
	Current string `json:"current"`
	List []MessageJson `json:"list"`
	Who map[string]chat.Client `json:"who"`
}

/**
发送消息
*/
func wwwChatSend() {
	content := strings.Join(rr.Form["content"], "")
	to_key := strings.Join(rr.Form["to_key"], "")
	//outMsg("你要发给谁？","")
	initDb()
	cc.Db = fdb
	defer cc.Db.Close()
	//checkError(err)

	id, err := cc.Send(content, sessionid, to_key)
	if id == 0 {
		outMsg(err.Error(), "")
		return
	} else {
		fmt.Println("SUCCESS:", id)
	}
	list, err := cc.List(to_key, id, 10)
	checkError(err)
	//fmt.Println(list)
	m := MessageList{Status: 0, Chat_id: id}
	//var v chat.Message
	for _, v := range list {
		//fmt.Println(v.Chat_id)
		m.List = append(m.List, MessageJson{v.Chat_id, v.From_key, v.To_key, v.Content, v.Add_time, time.Unix(v.Add_time, 0).Format("2006/01/02 15:04:05"), v.From_key == sessionid})
	}
	out, _ := json.Marshal(m)
	//fmt.Println("===============")
	fmt.Fprintf(rw, jsonp(string(out)))
	//fmt.Println(content,to_key)
	//fmt.Println(time.Now().Format("2006 年 01 月 02 日 15:04:05 Z"))

}

func wwwChatList() {
	//	chat_id_tmp := strings.Join(rr.Form["chat_id"],"")
	initDb()
	cc.Db = fdb
	defer cc.Db.Close()
	chat_id_tmp := rr.Form.Get("chat_id")
	var chat_id int64
	if chat_id_tmp != "" {
		var err error
		chat_id, err = strconv.ParseInt(chat_id_tmp, 10, 0)
		checkError(err)
	} else {
		chat_id = 0
	}
	//	chat_id := int64(chat_id_tmp)
	fmt.Println("chat_id:", chat_id)
	list, err := cc.List(sessionid, chat_id, 50)
	checkError(err)
	//fmt.Println(list)
	m := MessageList{Status: 0, Chat_id: 0,Who:Who,Current:sessionid}
	//var v chat.Message
	for _, v := range list {
		//fmt.Println(v.Chat_id)
		m.List = append(m.List, MessageJson{v.Chat_id, v.From_key, v.To_key, v.Content, v.Add_time, time.Unix(v.Add_time, 0).Format("2006/01/02 15:04:05"), v.From_key == sessionid})
	}
	out, _ := json.Marshal(m)
	//fmt.Println("===============")
	fmt.Fprintf(rw, jsonp(string(out)))
}

/**
改名控制器
*/
func wwwChatChangeName() {
	initDb()
	cc.Db = fdb
	defer cc.Db.Close()
	name := strings.Join(rr.Form["name"], "")
	//fmt.Println(name)
	if name == "" {
		outMsg("新昵称不能为空", "")
		return
	}
	Who = cc.ChangeName(Who, sessionid, name)
	outMsg("", "昵称修改成功")
	fmt.Println(Who)
	return
}

/**
  输出在线的
*/
func wwwChatWho() {
	j, _ := json.Marshal(Who)
	fmt.Fprintf(rw, jsonp(string(j)))
}

/**
输出jsonp的格式
*/
func jsonp(s string) string {
	callback := strings.Join(rr.Form["callback"], "")
	if callback != "" {
		return callback + "(" + s + ")"
	} else {
		return s
	}
}

/**
 * 输出json错误
 */
func outMsg(err string, msg string) {
	var s outputMsg
	if err != "" {
		s = outputMsg{Err: err, Msg: "", Status: -1}
	} else if msg != "" {
		s = outputMsg{Err: "", Msg: msg}
	} else {
		s = outputMsg{Err: "未知错误", Msg: ""}
	}
	j, err2 := json.Marshal(s)
	checkError(err2)
	fmt.Fprintf(rw, jsonp(string(j)))
}

/**
在线变量
*/
var Who = make(map[string]chat.Client, 5)

/**
Session测试
*/
func wwwSession() {

	ct := sess.Get("countnum")
	if ct == nil {
		sess.Set("countnum", 1)
		ct = 1
	} else {
		sess.Set("countnum", (ct.(int) + 1))
	}
	t, _ := template.ParseFiles("themes/session.html")
	rw.Header().Set("Content-Type", "text/html")
	t.Execute(rw, sess.Get("countnum"))
}

/**
session初始化
*/
func initSession() {
	var err error
	globalSessions, err = session.NewManager("memory", "gosessionid", chat.LEFTTIME)
	checkError(err)
	go globalSessions.GC()
	sess = globalSessions.SessionStart(rw, rr)
	//下面这种方式获取session才是对的。
	sessionid = sess.SessionID()
	//	cookie,err := rr.Cookie("gosessionid")
	//checkError(err)
	//fmt.Println(cookie)
	//	if err == nil {
	//		sessionid = cookie.Value
	//	} else {
	//		sessionid = ""
	//	}
	fmt.Println(sessionid)
	Who = chat.Online(Who, sessionid)
	//Who = chat.ChangeName(Who,sessionid,"小李子")
	//sess.Set("Cool",15)
	//l := sess.Get("Cool")
	//fmt.Println("-----------------------")
	//fmt.Println("Online :",len(Who))
	//fmt.Println(Who)
}

func initDb() {
	var err error
	fdb, err = sql.Open("mysql", "goserver:goserver123@/goserver?charset=utf8")
	checkError(err)
	fmt.Println("数据库链接加载完成", fdb)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	fmt.Println("Start goserver in ", port)
	// http.HandleFunc("/log",www_log)
	// http.HandleFunc("/log/",www_log)
	http.HandleFunc("/", www)
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
